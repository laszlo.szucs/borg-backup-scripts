#!/bin/sh

# This is a helper script for the borg backup software. The backups need to be 
# created manually using the borg-backup.sh script. Here we only provide 
# reminder to perform the backup regularly.

# Notification is displayed if the last successful backup is older than $back_freq
back_freq=4
date_file="${HOME}/.config/borg/latest-successful-backup"

# Check whether the timer file exists
if [ -f "${date_file}" ]
then
   echo "Borg Backup Reminder: file found: ${date_file}."
else
   echo "Borg Backup Reminder: file not found: ${date_file}."
   notify-send "Borg Backup Reminder" "No last backup date information found. Please rerun the borg-backup.sh script and/or check that ${timer_file} contains a valied date." -t 60000
   exit 0
fi

# Get the date of the last successful backup and the current date in seconds since 1970-01-01.
last_date=$(<${date_file})
last_sec=$(date --date="${last_date}" +"%s")
last_reminder=$(date --date="${last_date}" +"%a %b %d %Y")
current_sec=$(date +"%s")

# Convert seconds to days.
last_day=$((${last_sec} / 86400))
current_day=$((${current_sec} / 86400))

# Check if the latest backup was in the previous year.
if [ ${last_day} -gt ${current_day} ];
then
   current_day=$(($current_day + 365))
fi

# Notify if the last backup is older than ${back_freq}
# TODO: add an option to click on the notification to start the backup.
if [ $((${current_day} - ${last_day})) -ge ${back_freq} ];
then
   echo "Borg Backup Reminder: Backup is older then ${back_freq} days, please run the borg-backup.sh script."
   notify-send "Borg Backup Reminder" "Backup is older then ${back_freq} days, please run the borg-backup.sh script! Last successful backup was on ${last_reminder}." -t 60000
fi

# Return sucessfuly
global_exit=0

exit ${global_exit}
