#!/bin/sh

# Helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

# Setting this, so the repo does not need to be given on the commandline:
export BORG_REPO=/run/media/laszlo/Athena/borg-backup-rhea

# Setting this, so you won't be asked for your repository passphrase:
#export BORG_PASSPHRASE='XYZl0ngandsecurepa_55_phrasea&&123'
# or this to ask an external program to supply the passphrase:
export BORG_PASSCOMMAND='secret-tool lookup user laszlo domain athena'
# On KDE based system use kwallet instead:
if [ -z $( eval ${BORG_PASSCOMMAND} ) ];
then
    info "Password not found in secret-tool, trying KWallet."
    export BORG_PASSCOMMAND='kwallet-query kdewallet --folder borg-backup -r athena'
fi

info "Starting backup"

# Notify user via notify-osd as well
notify-send "Borg Backup" "Starting backup to Athena."

# Backup the most important directories into an archive named after
# the machine this script is currently running on:

borg create                         \
    --verbose                       \
    --filter AME                    \
    --list                          \
    --stats                         \
    --show-rc                       \
    --compression lz4               \
    --exclude-caches                \
    --exclude-from ${HOME}'/.config/borg/exclude-list' \
                                    \
    ::'{hostname}-{now}'            \
    /home/laszlo                    \

backup_exit=$?

info "Pruning repository"

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machines' archives also:

borg prune                          \
    --list                          \
    --prefix '{hostname}-'          \
    --show-rc                       \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  6               \

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 0 ];
then
    info "Backup and Prune finished successfully"
    notify-send "Borg Backup" "Backup and Prune finished successfully."
    date +"%F %T %Z" > ${HOME}/.config/borg/latest-successful-backup
fi

if [ ${global_exit} -eq 1 ];
then
    info "Backup and/or Prune finished with a warning"
    notify-send "Borg Backup" "Backup and/or Prune finished with a warning."
fi

if [ ${global_exit} -gt 1 ];
then
    info "Backup and/or Prune finished with an error"
    notify-send "Borg Backup" "Backup and/or Prune finished with an error."  --urgency=critical
fi

exit ${global_exit}
